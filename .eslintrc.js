module.exports = {
    "extends": ["airbnb-base", "plugin:react/recommended"],
    "plugins": [
      "prefer-import"
    ],
    rules: {
      "indent": ["error", "tab"],
      "no-tabs": 0,
      "linebreak-style": ["error","windows"],
      "prefer-import/prefer-import-over-require": ["error"],
      "import/prefer-default-export": 0,
      "linebreak-style": 0,

    }
  };
  