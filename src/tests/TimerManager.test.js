import React from 'react';
import Adapter from 'enzyme-adapter-react-16'
import enzyme from 'enzyme';

import {TimeManager} from '../containers/TimeManager';
enzyme.configure({adapter: new Adapter()});

it('should return work duration when not on break', () => {
	const props = {
		addSecond: jest.fn(),
		reset: jest.fn(),
		toggleBreak: jest.fn(),
		addFinished: jest.fn(),
		seconds: 0,
		isBreak: false,
		isLongBreak: true,
		timerCount: 0,

	}
	const wrapper = enzyme.shallow(<TimeManager {...props}/>);
	wrapper.setState({workDuration: 1337});
	expect(wrapper.instance().getTimerLength()).toEqual(1337);
	// wrapper.find('input').simulate('click');
	// expect(props.reset.mock.calls).toMatchSnapshot();
});
