import React from 'react';
import Adapter from 'enzyme-adapter-react-16'
import enzyme from 'enzyme';
import TimerReset from '../components/TimerReset';

enzyme.configure({adapter: new Adapter()});

describe('When I click the reset button', () => {
	it('should call the reset method', () => {
		const props = {
			reset: jest.fn(),
			isBreak: false,
		}
		const wrapper = enzyme.shallow(<TimerReset {...props}/>);
		wrapper.find('input').simulate('click');
		expect(props.reset.mock.calls).toMatchSnapshot();
	});

	it('should render if not break', () => {
		const props = {
			reset: jest.fn(),
			isBreak: false,
		}
		const wrapper = enzyme.shallow(<TimerReset {...props}/>);
		expect(wrapper.find('input').props().style.display).toEqual('block');
	});

	it('should not render if break', () => {
		const props = {
			reset: jest.fn(),
			isBreak: true,
		}
		const wrapper = enzyme.shallow(<TimerReset {...props}/>);
		expect(wrapper.find('input').props().style.display).toEqual('none');
	});
});