export const addSecond = () => ({
	type: 'addSecond',
});

export const reset = () => ({
	type: 'reset',
});

export const addFinished = () => ({
	type: 'addFinished',
});

export const toggleBreak = () => ({
	type: 'toggleBreak',
});
