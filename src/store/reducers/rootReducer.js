import { combineReducers } from 'redux';
import currentTimer from './currentTimer';
import timerCount from './timerCount';

export default combineReducers({
	currentTimer,
	timerCount,
});
