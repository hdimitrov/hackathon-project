export default (state = { timers: 1 }, action) => {
	switch (action.type) {
	case 'addFinished':
		return {
			...state,
			timers: state.timers + 1,
		};
	default:
		return state;
	}
};
