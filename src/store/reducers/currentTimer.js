export default (state = { time: 0, isBreak: false }, action) => {
	switch (action.type) {
	case 'addSecond':
		return {
			...state,
			time: state.time + 1,
		};
	case 'reset':
		return {
			...state,
			time: 0,
		};
	case 'toggleBreak':
		return {
			...state,
			isBreak: !state.isBreak,
		};
	default:
		return state;
	}
};
