import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TimerReset extends Component {
  static propTypes = {
    reset: PropTypes.func.isRequired,
    isBreak: PropTypes.bool.isRequired,
  };
  	render = () => {
		return (
			<div>
				<input style={{display: this.props.isBreak ? 'none' : 'block'}} type="submit" value="Reset" onClick={this.props.reset} />
			</div>
		);
	}
}


export default TimerReset;
