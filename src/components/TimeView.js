import React, { Component } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';

class TimeView extends Component {
  static propTypes = {
    seconds: PropTypes.number.isRequired,
    isBreak: PropTypes.bool.isRequired,
    timerLength: PropTypes.number.isRequired,
  };

  convertToString = () => {
    // ref: https://stackoverflow.com/questions/31337370/how-to-convert-seconds-to-hhmmss-in-moment-js
    if(this.props.timerLength > 3600) {
      return moment('2015-01-01').startOf('day')
      .seconds(this.props.timerLength - this.props.seconds)
      .format('h:mm:ss');
    } else {
      return moment('2015-01-01').startOf('day')
      .seconds(this.props.timerLength - this.props.seconds)
      .format('mm:ss');
  
    }
  }
  

	render = () => {
		return (
			<div>
				<h1 style={{color: this.props.isBreak ? 'red' : 'blue'}}>{ this.convertToString() }</h1>
			</div>
		);
	}
}


export default TimeView;
