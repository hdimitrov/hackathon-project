import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TimerStartStop extends Component {
  static propTypes = {
    isPaused: PropTypes.bool.isRequired,
    startTimer: PropTypes.func.isRequired,
    stopTimer: PropTypes.func.isRequired,
  };
  
  toggle = () => {
    this.props.isPaused ? this.props.startTimer() : this.props.stopTimer();
  }
	render = () => {
		return (
			<div>
				<input type="submit" value={this.props.isPaused ? 'Resume' : 'Pause'} onClick={this.toggle} />
			</div>
		);
	}
}


export default TimerStartStop;
