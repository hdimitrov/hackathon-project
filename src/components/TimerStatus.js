import React, { Component } from 'react';
import PropTypes from 'prop-types';

class TimerStatus extends Component {
  static propTypes = {
    isLongBreak: PropTypes.bool.isRequired,
    isBreak: PropTypes.bool.isRequired,
    timerCount: PropTypes.number.isRequired,
  };

  status = () => {
    return this.props.isBreak ? (this.props.isLongBreak ? 'Long Break' : 'Short Break') : 'Working';
  };
	render = () => {
		return (
			<div>
				<h1>{ this.status() }</h1>
        <h2>Number of timers finished: { this.props.timerCount}</h2>
			</div>
		);
	}
}


export default TimerStatus;
