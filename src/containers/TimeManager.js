import React, { Component } from 'react';

import { connect } from 'react-redux';
import { addSecond, reset, toggleBreak, addFinished } from '../store/actions/actions';
import TimeView from '../components/TimeView';
import TimeStartStop from '../components/TimerStartStop';
import TimerReset from '../components/TimerReset';
import TimerStatus from '../components/TimerStatus';
import PropTypes from 'prop-types'
import { Link, Route, Switch } from "react-router-dom";

export class TimeManager extends Component {
	state = {
		timer: -1,
		isPaused: true,
		workDuration: 1500,
		longBreakDuration: 1800,
		shortBreakDuration: 300,
	};

	getTimerLength = () => {
		if(this.props.isBreak) {
			return this.props.isLongBreak ? this.state.longBreakDuration : this.state.shortBreakDuration;
		}
		return this.state.workDuration;
	};
	startTimer = () => {
		this.setState({timer: setInterval(this.tick, 1), isPaused: false});
	};

	tick = () => {
		this.props.addSecond();
		if(this.getTimerLength() === this.props.seconds) {
			if(this.props.isBreak) {
				this.props.addFinished();
				this.stopTimer();
			}
			this.props.toggleBreak();
			this.props.reset();
		}
	}
	stopTimer = () => {
		clearInterval(this.state.timer);
		this.setState({isPaused: true});
	};
	reset = () => {
		this.stopTimer();
		this.props.reset();
	};

	render() {
		return (
			<div>
				<Link to="/">Home</Link>
				<Link to="/settings">Settings</Link>
				<Link to="/penguin">Penguins!</Link>

				<Switch>
					<Route path="/settings">
					Work duration: <input type="string" value={this.state.workDuration} placeholder="Work duration" onChange={(ev) => this.setState({workDuration: ev.target.value})} />
					Long break duration: <input type="string" value={this.state.longBreakDuration} placeholder="Long break duration" onChange={(ev) => this.setState({longBreakDuration: ev.target.value})} />
					Short break duration 				: <input type="string" value={this.state.shortBreakDuration} placeholder="Short Break duration" onChange={(ev) => this.setState({shortBreakDuration: ev.target.value})} />
					</Route>
					<Route exact path="/">
						<TimerStatus isBreak={this.props.isBreak} isLongBreak={this.props.isLongBreak} timerCount={this.props.timerCount} />
						<TimeView timerLength={this.getTimerLength()} isBreak={this.props.isBreak} seconds={this.props.seconds}/>
						<TimeStartStop isPaused={this.state.isPaused} startTimer={this.startTimer} stopTimer={this.stopTimer} />
						<TimerReset isBreak={this.props.isBreak} reset={this.reset} />
					</Route>
					<Route path="*">
						FUCK
					</Route>
				</Switch>

				
			</div>
		);
	}

	static propTypes = {
		addSecond: PropTypes.func.isRequired,
		reset: PropTypes.func.isRequired,
		toggleBreak: PropTypes.func.isRequired,
		addFinished: PropTypes.func.isRequired,
		seconds: PropTypes.number.isRequired,
		isBreak: PropTypes.bool.isRequired,
		isLongBreak: PropTypes.bool.isRequired,
		timerCount: PropTypes.number.isRequired,

	  };
}

function mapDispatchToProps(dispatch) {
	return {
	  addSecond: () => dispatch(addSecond()),
	  reset: () => dispatch(reset()),
	  toggleBreak: () => dispatch(toggleBreak()),
	  addFinished: () => dispatch(addFinished()),
	};
}

function mapStateToProps(state) {
	return {
		seconds: state.currentTimer.time,
		isBreak: state.currentTimer.isBreak,
		isLongBreak: state.timerCount.timers % 4 === 0 && state.timerCount.timers !== 0,
		timerCount: state.timerCount.timers,
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(TimeManager);
