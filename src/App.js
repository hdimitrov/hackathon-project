import React from 'react';
import TimeManager from './containers/TimeManager';

function App() {
	return (
		<div className="App">
			<TimeManager />
		</div>
	);
}

export default App;
